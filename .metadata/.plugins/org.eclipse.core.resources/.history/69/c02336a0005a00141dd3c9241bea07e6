package falstad;

import static org.junit.Assert.*;

import org.junit.Test;

import falstad.Robot.Direction;
//import com.sun.xml.internal.ws.handler.HandlerProcessor.Direction;
import falstad.Robot.Turn;

public class BasicRobotTest extends RobotMaze {
	
	RobotMaze TestMaze;
	
	/**
	 * Uses deterministic generation (det = true) to create a new MockMaze object with specified skill 
	 * and build it.
	 * @param skill the skill level
	 * @param det true will be deterministic
	 */
	void createMaze(int skill, boolean det) throws InterruptedException{
		this.TestMaze = new RobotMaze();
		this.TestMaze.method = 0;
		this.TestMaze.build(skill, det);
		this.TestMaze.mazebuilder.buildThread.join();
	}

	/**
	 * Correctly creates a new robot with proper attributes
	 * @param battery the initial battery level for the robot 
	 * @param mazeLevel the skill level for the robot's maze
	 * @return configured BasicRobot object
	 * @throws InterruptedException for threading
	 */
	public BasicRobot initRobot(float battery, int mazeLevel) throws InterruptedException {
		this.createMaze(mazeLevel, true);
		BasicRobot newRobot = new BasicRobot(battery, true, true, true, true, true, true);
		newRobot.setMaze(this.TestMaze);
		
		return newRobot;
	}

	/**
	 * Simply checks the robot's getBatteryLevel method with two simple configurations
	 * @throws InterruptedException for threading
	 */
	@Test
	public void testGetBatteryLevel() throws InterruptedException {
		BasicRobot robot1 = initRobot(2500, 4);
		assertTrue(robot1.getBatteryLevel() == 2500);
		
		BasicRobot robot2 = initRobot(1000, 4);
		assertTrue(robot2.getBatteryLevel() == 1000);
	}
	
	/**
	 * Tests two robots' setBatteryLevel methods, which are checked with getBatteryLevel
	 * @throws InterruptedException for threading
	 */
	@Test
	public void testSetBatteryLevel() throws InterruptedException {
		BasicRobot robot1 = initRobot(2500, 4);
		robot1.setBatteryLevel(2000);
		assertTrue(robot1.getBatteryLevel() == 2000);
		
		BasicRobot robot2 = initRobot(2500, 4);
		robot2.setBatteryLevel(robot2.getBatteryLevel() - 100);
		assertTrue(robot2.getBatteryLevel() == 2400);
	}
	
	/**
	 * Ensures that the energy for full 360 rotation is indeed 12
	 * @throws InterruptedException for threading
	 */
	@Test
	public void testGetEnergyForFullRotation() throws InterruptedException {
		BasicRobot robot1 = initRobot(2500, 4);
		assertTrue(robot1.getEnergyForFullRotation() == 12);
	}
	
	/**
	 * Ensures that the energy for one step forward is indeed 5
	 * @throws InterruptedException for threading
	 */
	@Test
	public void testGetEnergyForStepForward() throws InterruptedException {
		BasicRobot robot1 = initRobot(2500, 4);
		assertTrue(robot1.getEnergyForStepForward() == 5);
	}
	
	/**
	 * Configures a robot without a backward or left sensor, then tests the checker for all four directions
	 * @throws InterruptedException for threading
	 */
	@Test
	public void testHasDistanceSensor() throws InterruptedException {
		BasicRobot robot1 = new BasicRobot(2500, true, true, true, false, false, true);
		
		assertTrue(robot1.hasDistanceSensor(falstad.Robot.Direction.FORWARD));
		assertFalse(robot1.hasDistanceSensor(falstad.Robot.Direction.BACKWARD));
		assertFalse(robot1.hasDistanceSensor(falstad.Robot.Direction.LEFT));
		assertTrue(robot1.hasDistanceSensor(falstad.Robot.Direction.RIGHT));
	}
	
	/**
	 * Configures two robots, one with a room sensor and one without, and then checks the hasRoomSensor method
	 * @throws InterruptedException for threading
	 */
	@Test
	public void testHasRoomSensor() throws InterruptedException {
		BasicRobot robot1 = new BasicRobot(2500, true, false, true, true, true, true);
		BasicRobot robot2 = new BasicRobot(2500, true, true, true, true, true, true);
		
		assertFalse(robot1.hasRoomSensor());
		assertTrue(robot2.hasRoomSensor());
	}
	
	/**
	 * Configures two robots, one with a junction sensor and one without, and then checks the hasJunctionSensor method
	 * @throws InterruptedException for threading
	 */
	@Test
	public void testHasJunctionSensor() throws InterruptedException {
		BasicRobot robot1 = new BasicRobot(2500, false, true, true, true, true, true);
		BasicRobot robot2 = new BasicRobot(2500, true, true, true, true, true, true);
		
		assertFalse(robot1.hasJunctionSensor());
		assertTrue(robot2.hasJunctionSensor());
	}
	
	/**
	 * Navigates to a known junction (because of deterministic generation), and then tests the isAtJunction method. 
	 * Also tests the method when we are at a known position that isn't a junction.
	 * @throws InterruptedException for threading
	 */
	@Test
	public void testIsAtJunction() throws InterruptedException {
		BasicRobot robot1 = initRobot(2500, 4);
		BasicRobot robot2 = new BasicRobot(2500, false, true, true, true, true, true);
		
		try {
			robot2.isAtJunction();
			assertTrue(false);
		} catch (UnsupportedOperationException e1) {
			assertTrue(true);
		}
		
		assertFalse(robot1.isAtJunction());
		
		try {
			robot1.rotate(Turn.RIGHT);
			robot1.move(1);
			robot1.rotate(Turn.LEFT);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		assertTrue(robot1.isAtJunction());
	}
	
	/**
	 * Tests both conditions for the hasStopped method, running out of energy and hitting a known wall.
	 * @throws InterruptedException for threading
	 */
	@Test
	public void testHasStopped() throws InterruptedException {
		BasicRobot robot1 = initRobot(2500, 4);
		assertTrue(robot1.hasStopped());
		try {
			robot1.rotate(Turn.RIGHT);
		} catch (Exception e) {
			e.printStackTrace();
		}
		assertFalse(robot1.hasStopped());
		
		robot1.setBatteryLevel(0);
		assertTrue(robot1.hasStopped());
	}
	
	/**
	 * Navigates to a known room (because of deterministic generation), and then tests the isInsideRoom method. 
	 * Also tests the method when we are at a known position that isn't a room.
	 * Additionally, tests the exception with no sensor.
	 * @throws InterruptedException for threading
	 */
	@Test
	public void testIsInsideRoom() throws InterruptedException {
		BasicRobot robot1 = initRobot(2500, 2);
		BasicRobot robot2 = new BasicRobot(2500, true, false, true, true, true, true);
		
		try {
			robot2.isInsideRoom();
			assertTrue(false);
		} catch (UnsupportedOperationException e1) {
			assertTrue(true);
		}
		
		assertFalse(robot1.isInsideRoom());
		try {
			robot1.move(6);
			robot1.rotate(Turn.LEFT);
			robot1.move(2);
			robot1.rotate(Turn.LEFT);
			robot1.move(2);
			robot1.rotate(Turn.LEFT);
		} catch (Exception e) {
			e.printStackTrace();
		}
		assertTrue(robot1.isInsideRoom());
	}
	
	/**
	 * Tests a variety of move operations against known correct behavior
	 * @throws InterruptedException for threading
	 */
	@Test
	public void testMove() throws InterruptedException {
		BasicRobot robot1 = initRobot(2500, 2);
		BasicRobot robot2 = new BasicRobot(0, true, true, true, true, true, true);
		
		try {
			robot2.move(1);
			assertTrue(false);
		} catch (Exception e) {
			assertTrue(true);
		}
		
		int[] tempPos = null;
		
		try {
			tempPos = robot1.getCurrentPosition();
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		try {
			robot1.move(1);
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			assertTrue(tempPos[0] + 1 == robot1.getCurrentPosition()[0] && tempPos[1] == robot1.getCurrentPosition()[1]);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			tempPos = robot1.getCurrentPosition();
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		try {
			robot1.move(2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			assertTrue(tempPos[0] + 2 == robot1.getCurrentPosition()[0] && tempPos[1] == robot1.getCurrentPosition()[1]);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			robot1.rotate(Turn.LEFT);
			robot1.move(1);
			assertTrue(false);
		} catch (Exception e) {
			assertTrue(true);
			//e.printStackTrace();
		}
	}
	
	/**
	 * Tests a variety of rotate operations against known correct behavior
	 * @throws InterruptedException for threading
	 */
	@Test
	public void testRotate() throws InterruptedException {
		BasicRobot robot1 = initRobot(2500, 2);
		BasicRobot robot2 = new BasicRobot(0, true, true, true, true, true, true);
		
		try {
			robot2.rotate(Turn.AROUND);
			assertTrue(false);
		} catch (Exception e) {
			assertTrue(true);
		}
		
		try {
			robot1.rotate(Turn.LEFT);
		} catch (Exception e) {
			e.printStackTrace();
		}
		assertTrue(robot1.Curr_Cardinal == Robot.Cardinal.SOUTH);
		
		try {
			robot1.rotate(Turn.LEFT);
		} catch (Exception e) {
			e.printStackTrace();
		}
		assertTrue(robot1.Curr_Cardinal == Robot.Cardinal.WEST);
		
		try {
			robot1.rotate(Turn.LEFT);
		} catch (Exception e) {
			e.printStackTrace();
		}
		assertTrue(robot1.Curr_Cardinal == Robot.Cardinal.NORTH);
		
		try {
			robot1.rotate(Turn.RIGHT);
		} catch (Exception e) {
			e.printStackTrace();
		}
		assertTrue(robot1.Curr_Cardinal == Robot.Cardinal.WEST);
		
		try {
			robot1.rotate(Turn.AROUND);
		} catch (Exception e) {
			e.printStackTrace();
		}
		assertTrue(robot1.Curr_Cardinal == Robot.Cardinal.EAST);
	}
	
	/**
	 * Tests the getCurrentDirection getter against known correct data
	 * @throws InterruptedException for threading
	 */
	@Test
	public void testGetCurrentDirection() throws InterruptedException {
		BasicRobot robot1 = initRobot(2500, 4);
		int[] curDirection = robot1.getCurrentDirection();
		
		//System.out.println(curDirection[0]);
		//System.out.println(curDirection[1]);
		assertTrue(curDirection[0] == 1);
		assertTrue(curDirection[1] == 0);
	}
	
	/**
	 * Tests the distanceToObstacle method against known correct data
	 * @throws InterruptedException for threading
	 */
	@Test
	public void testDistanceToObstacle() throws InterruptedException {
		BasicRobot robot1 = initRobot(2500, 4);

		try {
			robot1.rotate(Turn.RIGHT);
			assertTrue(robot1.distanceToObstacle(Direction.FORWARD) == 2);
		} catch (Exception e) {
			//e.printStackTrace();
		}
	}	
}