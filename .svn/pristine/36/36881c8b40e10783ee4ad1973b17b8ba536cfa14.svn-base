package edu.wm.cs.cs301.slidingpuzzle;
import static org.junit.Assert.*;

import java.util.Random;
import java.util.logging.Logger;

import org.junit.Test;

import edu.wm.cs.cs301.slidingpuzzle.Operation;
import edu.wm.cs.cs301.slidingpuzzle.PuzzleSolver;
import edu.wm.cs.cs301.slidingpuzzle.PuzzleState;
import edu.wm.cs.cs301.slidingpuzzle.SimplePuzzleSolver;

/**
 * Bundle of test cases to verify the puzzle solver.
 * @author Probir, Peter Kemper, John Sawin, Maurice Hayward
 *
 */

public class MySimplePuzzleSolverTest {
	
static final Logger LOG = Logger.getLogger(StateAndSolverTest.class.getName());
	
	/**
	 * Wrapper to encapsulate constructor call for class that implements PuzzleState interface
	 * @param puzzle
	 * @return
	 */
	PuzzleState createPuzzleState(int[] puzzle) {
		PuzzleState pState= new SimplePuzzleState();
	    pState.configureState(puzzle);
	    return pState ;
	}
	
	// a few particular puzzles to test
	// 2 8 3
	// 1 6 4
	// 7 5 0
	final int[] puzzleLowerRightCorner = {2,8,3,1,6,4,7,5,0};
	// 2 8 3
	// 1 6 4
	// 7 0 5	
	final int[] puzzleLastRowMiddlePosition = {2,8,3,1,6,4,7,0,5};
	// 2 8 3	
	// 1 0 4 
	// 7 6 5	
	final int[] puzzleCenterPosition = {2,8,3,1,0,4,7,6,5};
	// 2 0 3
	// 1 6 4 
	// 7 8 5
	final int[] puzzleFirstRowMiddlePosition = {2,0,3,1,6,4,7,8,5};
	// 2 0 3
	// 1 8 4 
	// 7 6 5
	final int[] puzzleFirstRowMiddlePosition2 = {2,0,3,1,8,4,7,6,5};
	// 2 8 3
	// 1 6 4
	// 0 7 5 
	final int[] puzzleLowerLeftCorner = {2,8,3,1,6,4,0,7,5} ;
	// 0 8 3
	// 1 6 4
	// 2 7 5 
	final int[] puzzleUpperLeftCorner = {0,8,3,1,6,4,2,7,5} ;
	// 8 3 0
	// 1 6 4
	// 2 7 5 	
	final int[] puzzleUpperRightCorner = {8,3,0,1,6,4,2,7,5} ;
	
	// some 2 x 2 puzzles
	// note that puzzles are not necessarily reachable from each other
	final int[] smallPuzzleUpperLeftCorner = {0,1,2,3};
	final int[] smallPuzzleLowerLeftCorner = {1,3,0,2};
	final int[] smallPuzzleLowerRightCorner = {3,1,2,0}; 
	final int[] smallPuzzleUpperRightCorner = {1,0,2,3};
	
	final int[] a3x3Puzzle2 = {0, 1, 2, 3, 4, 5, 6, 7, 8};
	final int[] a3x3Puzzle1 = {3, 4, 1, 6, 7, 0, 8, 2, 5};
	
	/**
	 * Produces a string with the content of a puzzle, used for error messages
	 * @param puzzle
	 * @return
	 */
	private String printPuzzle(int[] puzzle) {
		String result = "" ;
		for (int i = 0 ; i < puzzle.length ; i++) {
			result += " " + puzzle[i] ;
		}
		return result ;
	}
//-------------------------Test HELPER FUNCTION-----------------------------------------------//
	
	/**
	 * This test case verifies that solver can find route from initial state to goal state.
	 * This test case configure the solver with valid 2x2 initial and goal state. The movesToSolve function 
	 * is called to verify the list moves the solver found to reach the goal state.
	 */	
	@Test
	public void testSolver2x2(){
		int[] puzzle1 = a3x3Puzzle1.clone();
		int[] puzzle2 = a3x3Puzzle2.clone();
	    
		PuzzleSolver pSolver = new SimplePuzzleSolver();
    	pSolver.configure(puzzle1, puzzle2);
		
	    PuzzleState goal = createPuzzleState(puzzle2) ;

	    checkThatComputedSolutionIsCorrect(pSolver, goal);
	}
	
	/**
	 * For a given, already configured solver, this method checks if the solver finds a solution and
	 * if that solution leads to the given goal
	 * @param pSolver
	 * @param goal
	 */
	private void checkThatComputedSolutionIsCorrect(PuzzleSolver pSolver, PuzzleState goal) {
		Operation[] moveList = pSolver.movesToSolve();
    	PuzzleState start = pSolver.getSolverInitialState();
		assertNotNull("Solver fails to find solution for starting state: " + 
				printPuzzle(start.getStateArray())  + " towards " + printPuzzle(goal.getStateArray()), 
				moveList) ;
    	PuzzleState result = performSequencesOfMoves(moveList, start);
 		assertTrue(goal.isEqual(result));
	}

	/**
	 * Performs a sequences of moves starting from the given state and returns the resulting state.
	 * @param moveList
	 * @param pState
	 * @return resulting state
	 */
	private PuzzleState performSequencesOfMoves(Operation[] moveList, PuzzleState pState) {	
		for(int i=0;i<moveList.length;i++)
		{
			switch (moveList[i]){
				case MOVEUP:
					if(pState!=null)
						pState = pState.moveUp();
					break;
				case MOVEDOWN:
					if(pState!=null)
						pState = pState.moveDown();
					break;
				case MOVELEFT:
					if(pState!=null)
						pState = pState.moveLeft() ;
					break;
				case MOVERIGHT:
					if(pState!=null)
						pState = pState.moveRight();
					break;
				default:
					fail("List of moves contained an unknown type element: " + moveList[i] + " at position " + i) ;
					break;
			}
			if(pState==null)
			{
				fail("List of moves contained illegal move at position: " + i) ;
			}
		}
		return pState;
	}
}
