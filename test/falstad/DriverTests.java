package falstad;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * Suite of tests for the new driver algorithms
 * @author John Sawin
 * @author Maurice Hayward
 *
 */
@RunWith(Suite.class)
@SuiteClasses({ GamblerTest.class, TremauxTest.class, WallFollowerTest.class,
		WizardTest.class })
public class DriverTests {

}
