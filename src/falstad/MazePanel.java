package falstad;

import falstad.Constants;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Panel;
import java.awt.FontMetrics;

public class MazePanel extends Panel  {
	/* Panel operates a double buffer see
	 * http://www.codeproject.com/Articles/2136/Double-buffer-in-standard-Java-AWT
	 * 
	 * for details
	 */
	Image bufferImage ;
	Graphics currentGraphic;
	private FontMetrics fm;

	public Font largeBannerFont = new Font("TimesRoman", Font.BOLD, 48);
	public Font smallBannerFont = new Font("TimesRoman", Font.BOLD, 16);

	public MazePanel() {
		super() ;
		this.setFocusable(false) ;
	}

	@Override
	public void update(Graphics g) {
		paint(g) ;
	}

	@Override
	public void paint(Graphics g) {
		g.drawImage(bufferImage,0,0,null) ;
	}

	/*
	public void setBufferImage(Image buffer) {
		bufferImage = buffer ;
	}
	 */

	public void initBufferImage() {
		bufferImage = createImage(Constants.VIEW_WIDTH, Constants.VIEW_HEIGHT);
		if (null == bufferImage)
		{
			System.out.println("Error: creation of buffered image failed, presumedly container not displayable");
		}
	}

	public void setBufferGraphics() {
		if (null == bufferImage)
			initBufferImage() ;
		this.currentGraphic = bufferImage.getGraphics();
		//return currentGraphic ;
	}

	public void update() {
		paint(getGraphics()) ;
	}

	public void setColor(Constants.Colors color){
		switch(color){
		case BLACK: currentGraphic.setColor(Color.black);
		break; 
		case DARKGREY:currentGraphic.setColor(Color.darkGray);
		break;
		case WHITE: currentGraphic.setColor(Color.white);
		break;
		case GREY: currentGraphic.setColor(Color.gray);
		break;
		case YELLOW: currentGraphic.setColor(Color.yellow);
		break;
		case RED: currentGraphic.setColor(Color.red);
		break;
		case BLUE: currentGraphic.setColor(Color.blue);
		break;
		case ORANGE: currentGraphic.setColor(Color.orange);
		break;
		default:
			break;
		}
	}

	public void setColor(Color col){
		currentGraphic.setColor(col);
	}
	public void fillRect(int x, int y, int width, int height){
		currentGraphic.fillRect(x, y, width, height);
	}

	public void fillPolygon(int xPoints[], int yPoints[],int nPoints){
		currentGraphic.fillPolygon(xPoints, yPoints, nPoints);
	}

	public void drawLine(int nx1, int ny1, int nx12, int ny2) {
		currentGraphic.drawLine(nx1, ny1, nx12, ny2);
	}

	public void fillOval(int i, int j, int cirsiz, int cirsiz2) {
		currentGraphic.fillOval(i, j, cirsiz, cirsiz2);
	}

	public int stringWidth(String str) {
		return fm.stringWidth(str);
	}

	public void fontMetrics() {
		fm = currentGraphic.getFontMetrics();
	}

	public void drawString(String str, int i, int ypos) {
		currentGraphic.drawString(str, i, ypos);
	}

	public void setFont(Font font) {
		currentGraphic.setFont(font);
		;
	}

	public Color getSegColor(int colorValue1, int colorValue2, int colorValue3) {
		return new Color(colorValue1, colorValue2, colorValue3);
	}
}