package falstad;

import java.awt.Container;
import java.awt.Event;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import falstad.Robot.Cardinal;
import falstad.Robot.Turn;

public class RobotKeyListener implements KeyListener {

	MazeApplication parent ;
	RobotDriver driver ;
	private int state;
	private float initialBattery;
	private int count;
	private boolean result = true;
	
	// fixing a value matching the escape key
	final int ESCAPE = 27;
	
	/**
	 * Constructor
	 * @param parent the parent container
	 * @param driver the driver to be used by the keylistener
	 */
	RobotKeyListener(MazeApplication parent, RobotDriver driver){
		this.parent = parent ;
		this.driver = driver ;
		this.initialBattery = this.driver.getRobot().getBatteryLevel();
	}
	
	/**
	 * Translates keyboard input to the corresponding characters for the Maze.keyDown method
	 */
	@Override
	public void keyPressed(KeyEvent arg0) {
		//System.out.println("1: Communicating key: " + arg0.getKeyText(arg0.getKeyCode()) + " with key char: " + arg0.getKeyChar() + " with code: " + arg0.getKeyCode());
		int key = arg0.getKeyChar() ;
		int code = arg0.getKeyCode() ;
		
		state = this.driver.getRobot().getMaze().state;

		switch (state) {
		case Constants.STATE_TITLE:
			break;
		case Constants.STATE_GENERATING:
			//this.driver.getRobot().setBatteryLevel(25);
			if (key == this.driver.getRobot().getMaze().ESCAPE) {
				this.driver.getRobot().getMaze().mazebuilder.interrupt();
				this.driver.getRobot().getMaze().buildInterrupted();
			}
			break;
		// if we are finished, return to initial state with title screen	
		case Constants.STATE_FINISH:
			this.driver.getRobot().getMaze().state = Constants.STATE_TITLE;
			this.driver.getRobot().getMaze().notifyViewerRedraw() ;
			this.driver.getRobot().getMaze().MazeView.cellCount = 0;
			this.driver.getRobot().getMaze().MazeView.energy = this.initialBattery;
			this.parent.reset();
			break;
		case Constants.STATE_DEAD:
			this.driver.getRobot().getMaze().state = Constants.STATE_TITLE;
			this.driver.getRobot().getMaze().notifyViewerRedraw() ;
			this.driver.getRobot().getMaze().MazeView.cellCount = 0;
			this.driver.getRobot().getMaze().MazeView.energy = this.initialBattery;
			this.parent.reset();
			break;
		case Constants.STATE_PLAY:
			
			this.updateCellCounter();
			this.updateEnergyConsumption();
			
			if (KeyEvent.CHAR_UNDEFINED == key)
			{
				if ((KeyEvent.VK_0 <= code && code <= KeyEvent.VK_9)||(KeyEvent.VK_A <= code && code <= KeyEvent.VK_Z))
					key = code ;
				if (KeyEvent.VK_ESCAPE == code)
					key = this.driver.getRobot().getMaze().ESCAPE;
				if (KeyEvent.VK_UP == code)
					key = 'k' ;
				if (KeyEvent.VK_DOWN == code)
					key = 'j' ;
				if (KeyEvent.VK_LEFT == code)
					key = 'h' ;
				if (KeyEvent.VK_RIGHT == code)
					key = 'l' ;
			}
			
			//System.out.println("Calling keydown with " + key) ;
			switch (key) {
			case Event.UP: case 'k': case '8':
				try {
					this.driver.getRobot().move(1);
					this.driver.getRobot().getMaze().MazeView.setCellCounter(this.driver.getCellCount());
					this.driver.getRobot().getMaze().MazeView.setEnergy(this.driver.getEnergyConsumption());
				} catch (Exception e) {
					//e.printStackTrace();
				};
				break;
			case Event.LEFT: case 'h': case '4':
				try {
					this.driver.getRobot().rotate(Turn.LEFT);
					//System.out.println(this.driver.getRobot().getBatteryLevel());
					//System.out.println(this.driver.getRobot().getMaze().MazeView.energy);
					updateEnergyConsumption();
				} catch (Exception e) {
					//e.printStackTrace();
				}
				break;
			case Event.RIGHT: case 'l': case '6':
				try {
					this.driver.getRobot().rotate(Turn.RIGHT);
					updateEnergyConsumption();
				} catch (Exception e) {
					//e.printStackTrace();
				}
				break;
			case Event.DOWN: case 'j': case '2':
				try {
					this.driver.getRobot().rotate(Turn.AROUND);
					updateEnergyConsumption();
				} catch (Exception e) {
					//e.printStackTrace();
				}
				break;
			case ESCAPE: case 65385:
				if (this.driver.getRobot().getMaze().solving)
					this.driver.getRobot().getMaze().solving = false;
				else
					this.parent.reset();
					this.driver.resetCellCounter();
					this.driver.getRobot().getMaze().state = Constants.STATE_TITLE;
				this.driver.getRobot().getMaze().notifyViewerRedraw() ;
				break;
			case ('w' & 0x1f): 
			{ 
				this.driver.getRobot().getMaze().setCurrentPosition(this.driver.getRobot().getMaze().px + this.driver.getRobot().getMaze().dx, this.driver.getRobot().getMaze().py + this.driver.getRobot().getMaze().dy) ;
				this.driver.getRobot().getMaze().notifyViewerRedraw() ;
				break;
			}
			case '\t': case 'm':
				this.driver.getRobot().getMaze().mapMode = !this.driver.getRobot().getMaze().mapMode; 		
				this.driver.getRobot().getMaze().notifyViewerRedraw() ;
				break;
			case 'z':
				this.driver.getRobot().getMaze().showMaze = !this.driver.getRobot().getMaze().showMaze; 		
				this.driver.getRobot().getMaze().notifyViewerRedraw() ; 
				break;
			case 's':
				this.driver.getRobot().getMaze().showSolution = !this.driver.getRobot().getMaze().showSolution; 		
				this.driver.getRobot().getMaze().notifyViewerRedraw() ;
				break;
			case ('s' & 0x1f):
				if (this.driver.getRobot().getMaze().solving)
					this.driver.getRobot().getMaze().solving = false;
				else {
					this.driver.getRobot().getMaze().solving = true;
				}
			break;
			case '+': case '=':
			{
				this.driver.getRobot().getMaze().notifyViewerIncrementMapScale() ;
				this.driver.getRobot().getMaze().notifyViewerRedraw() ; // seems useless but it is necessary to make the screen update
				break ;
			}
			case '-':
				this.driver.getRobot().getMaze().notifyViewerDecrementMapScale() ;
				this.driver.getRobot().getMaze().notifyViewerRedraw() ; // seems useless but it is necessary to make the screen update
				break ;
			}
			break;
			}
		
		parent.repaint() ;
		
		}

	private void resetBatteryAndCellCounter() {
		this.driver.getRobot().setBatteryLevel(this.initialBattery);
		this.driver.getRobot().getMaze().batteryLevel = this.initialBattery;
		this.driver.resetCellCounter();
	}

	private void updateCellCounter() {
		this.driver.cellCounter(1);
		this.driver.getRobot().getMaze().MazeView.cellCount = this.driver.getPathLength();
	} 	
	
	public void updateEnergyConsumption() {
		this.driver.getRobot().getMaze().MazeView.energy = this.driver.getEnergyConsumption();
		//System.out.println(this.driver.getRobot().getMaze().MazeView.energy);
	} 	
	
	public void setDriver(RobotDriver driver) {
		this.driver = driver;
	}
	
	@Override
	public void keyReleased(KeyEvent arg0) {
		// nothing to do
	}
	
	@Override
	public void keyTyped(KeyEvent arg0) {
		// NOTE FOR THIS TYPE OF EVENT IS getKeyCode always 0, so Escape etc is not recognized	
		// this is why we work with keyPressed
	}
}
