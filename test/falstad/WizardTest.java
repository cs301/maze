package falstad;

import static org.junit.Assert.*;

import org.junit.Test;

public class WizardTest {

RobotMaze TestMaze;
	
	/**
	 * Uses deterministic generation (det = true) to create a new MockMaze object with specified skill 
	 * and build it.
	 * @param skill the skill level
	 * @param det true will be deterministic
	 */
	private void createMaze(int skill, boolean det) throws InterruptedException{
		this.TestMaze = new RobotMaze();
		this.TestMaze.init();
		this.TestMaze.method = 0;
		this.TestMaze.build(skill, det);
		this.TestMaze.mazebuilder.buildThread.join();
	}
	
	/**
	 * Correctly creates a new robot with proper attributes
	 * @param battery the initial battery level for the robot 
	 * @param mazeLevel the skill level for the robot's maze
	 * @return configured BasicRobot object
	 * @throws InterruptedException for threading
	 */
	public BasicRobot initRobot(float battery, int mazeLevel) throws InterruptedException {
		this.createMaze(mazeLevel, true);
		BasicRobot newRobot = new BasicRobot(battery, true, true, true, false, true, false);
		newRobot.setMaze(this.TestMaze);
		
		return newRobot;
	}
	
	/**
	 * correctly sets up a robot driver
	 * @return a robot driver
	 * @throws InterruptedException
	 * @throws UnsuitableRobotException
	 */
	private RobotDriver setup(int skill, float battery) throws InterruptedException, UnsuitableRobotException {
		RobotDriver driver = new Wizard();
		BasicRobot newRobot = initRobot(battery, skill);
		newRobot.setMaze(this.TestMaze);
		driver.setRobot(newRobot);
		return driver;
	}
	
	/**
	 * checks to see if the algorithm can solve a very simple maze
	 * @throws Exception
	 */
	@Test
	public void testDriveToExit() throws Exception {
		RobotDriver wizard = setup(0, 2500);
		assertTrue(wizard.drive2Exit() == true);		
	}
	
	/**
	 * checks to see if the algorithm can solve a harder maze
	 * @throws Exception
	 */
	@Test
	public void testDriveToExitHarder() throws Exception {
		RobotDriver wizard = setup(2, 2500);
		try {
			assertTrue(wizard.drive2Exit() == true);
		} catch (Exception e) {
			assertTrue(true);
			System.out.println("Wizard could not solve the level 2 maze with the given energy!");
		}		
	}
	
	
	/**
	 * checks to see if the algorithm can solve an even harder maze
	 * @throws Exception
	 */
	@Test
	public void testDriveToExitHardest() throws Exception {
		RobotDriver wizard = setup(7, 2500);
		try {
			assertTrue(wizard.drive2Exit() == true);
		} catch (Exception e) {
			assertTrue(true);
			System.out.println("Wizard could not solve the level 7 maze with the given energy!");
		}		
	}
	
	/**
	 * checks the exception when the algorithm runs out of battery
	 * @throws InterruptedException
	 * @throws UnsuitableRobotException
	 */
	@Test
	public void testRunOutOfEnergy() throws InterruptedException, UnsuitableRobotException {
		RobotDriver wizard = setup(6, 40);
		try {
			wizard.drive2Exit();
		} catch (Exception e) {
			assertTrue(true);
		}
	}

}
