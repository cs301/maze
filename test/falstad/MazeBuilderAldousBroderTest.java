package falstad;

import static org.junit.Assert.*;

import org.junit.Test;


public class MazeBuilderAldousBroderTest extends MazeBuilderTest {
	
	/**
	 * Overrides createMaze (for deterministic generation) in order to use Aldous Broder (method = 2).
	 */
	void createMaze(int skill, boolean det) throws InterruptedException{
		this.TestMaze = new MockMaze();
		this.TestMaze.method = 2;
		this.TestMaze.build(skill, det);
		this.TestMaze.mazebuilder.buildThread.join();
	}
	
	/**
	 * Overrides createMaze (for random generation) in order to use Aldous Broder (method = 2).
	 */
	void createMaze(int skill) throws InterruptedException{
		this.TestMaze = new MockMaze();
		this.TestMaze.method = 2;
		this.TestMaze.build(skill);
		this.TestMaze.mazebuilder.buildThread.join();
	}
}