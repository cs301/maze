package edu.wm.cs.cs301.slidingpuzzle;

import java.util.*;

public class SimplePuzzleSolver implements PuzzleSolver {

	
	public int[] initial;
	public int[] goal;
	public int dimension;
	public PuzzleState initialState;
	public PuzzleState goalState;
	public int usedListSize;
	public ArrayDeque<SimplePuzzleState> workingQueue;
	
	
	@Override
	public boolean configure(int[] initial, int[] goal) {		
		// Psuedocode
		// if int[] initial = 0:
		// 	 return false
		// else:
		//   initial = int[] initial
		//
		// if int[] goal = 0:
		// 	 return false
		// else:
		//   goal = int[] goal
		//
		// return true
		
		if (initial == null) {
			return false;
		}else{
			this.initial = initial;
			
		}
		
		if (goal == null) {
			return false;
		}else{
			this.goal = goal;
		}
		
		this.initialState = this.getSolverInitialState();
		this.goalState = this.getSolverFinalState();
		this.dimension = (int) Math.sqrt(this.initialState.getStateArray().length);
		
		return true;
	}
	
	
	@Override
	public Operation[] movesToSolve() {

		/*
		 * Psuedocode
		 * workingQueue = empty double ended queue
		 * workingQueue.add(initialState)
		 * usedList = empty list
		 * while length of workingQueue != 0:
		 * 		currentState = workingQueue.pop
		 * 		usedList.append(currentState)
		 * 
		 * 		if currentState == goalState:
		 * 			exit while loop
		 * 		
		 * 		create all possible children states
		 * 		for each child state:
		 * 			if state == null or state in usedList:
		 * 				pass
		 * 			else:
		 * 				workingQueue.add(child state)
		 * 
		 * solvedList = empty list
		 * while currentState != None:
		 * 		solvedList.append(currentState)
		 * 		currentState = currentState.parent
		 * 
		 * return solvedList
		 */
		
		this.workingQueue = new ArrayDeque<SimplePuzzleState>();
		this.workingQueue.addFirst(this.getSolverInitialState());
		
		ArrayList<SimplePuzzleState> usedList = new ArrayList<SimplePuzzleState>(); 
		
		SimplePuzzleState currentState = null;
		int testCount = 0;
		
		while (workingQueue.size() > 0) {
			currentState = workingQueue.removeLast();
			usedList.add(currentState);
			
			// break upon impossible puzzle
			if (testCount == this.getMaxSizeOfQueue()) {
				break;
			}
			
			// if goal state is found, construct return list
			if (currentState.isEqual(this.goalState)) {
				ArrayList<Operation> solvedList = new ArrayList<Operation>(); 	
				
				// travel back up each parent state, adding to list as we go
				while (currentState.getParent() != null) {
					solvedList.add(currentState.lastMove);
					currentState = currentState.getParent();
				}
				
				// reverse solved list to be in proper order
				Collections.reverse(solvedList);
				
				Operation[] solvedListOperation;
				solvedListOperation = new Operation[solvedList.size()];
				
				// add enums to solve list
				for(int i=0; i < solvedList.size(); i++){
					solvedListOperation[i] = solvedList.get(i);
				}
				
				this.usedListSize = usedList.size();
				
				return solvedListOperation;
			}
			
			// Create all possible children states
			SimplePuzzleState upState = currentState.moveUp();
			SimplePuzzleState downState = currentState.moveDown();
			SimplePuzzleState leftState = currentState.moveLeft();
			SimplePuzzleState rightState = currentState.moveRight();
			
			// Test if "up" child is valid, or already visited
			if (upState == null|| isInList(upState, usedList)) {}
			else {
				testCount++;
				workingQueue.addFirst(upState);
			}
			
			// Test if "down" child is valid, or already visited
			if (downState == null || isInList(downState, usedList)) {}
			else {
				testCount++;
				workingQueue.addFirst(downState);
			}
			
			// Test if "left" child is valid, or already visited
			if (leftState == null || isInList(leftState, usedList)) {}
			else {
				testCount++;
				workingQueue.addFirst(leftState);
			}
			
			// Test if "right" child is valid, or already visited
			if (rightState == null || isInList(rightState, usedList)) {}
			else {
				testCount++;
				workingQueue.addFirst(rightState);
			}
		}	
		return null;	
	}

	
	@Override
	public SimplePuzzleState getSolverInitialState() {
		return createPuzzleState(this.initial);
	}

	
	@Override
	public SimplePuzzleState getSolverFinalState() {
		return createPuzzleState(this.goal);
	}

	
	@Override
	public int getNumberOfStatesExplored() {
		return this.usedListSize;
	}

	
	@Override
	public int getMaxSizeOfQueue() {
		return getFactorial((int)Math.pow(this.dimension, 2)) / 2;
	}
	
	
	// Helper Function to create proper puzzle states
	SimplePuzzleState createPuzzleState(int[] puzzle) {
		SimplePuzzleState pState= new SimplePuzzleState();
	    pState.configureState(puzzle);
	    return pState;
	}
	
	
	// Helper Function to calculate factorial
	int getFactorial(int number) {
		if (number <= 0) {
			return 0;
			}
		int factNumber = number - 1;
		int returnNumber = number;
		while (factNumber > 0) {
			returnNumber = returnNumber * factNumber;
			factNumber = factNumber - 1;
		}
		return returnNumber;
	}
	
	
	// Helper Function to determine if a given puzzle state is within a given ArrayList
	boolean isInList(SimplePuzzleState puzzle, ArrayList<SimplePuzzleState> list) {
		int length = list.size();
		for (int i = 0; i < length; i++) {
			if (puzzle.isEqual(list.get(i))) {return true;}
		}
		return false;
	}
}