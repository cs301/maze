package falstad;

import falstad.Constants.Colors;

//import java.awt.Color;
//import java.awt.Font;
//import java.awt.FontMetrics;
//import java.awt.Graphics;
//import java.awt.*;
//import java.awt.event.*;
//import javax.swing.*;

public class MazeView extends DefaultViewer {

	Maze maze ; // need to know the maze model to check the state 
	int cellCount;
	public float energy;
	public float initEnergy;
	public int driverType = 0 ;
	// and to provide progress information in the generating state
	
	public MazeView(Maze m) {
		super() ;
		maze = m;
		cellCount = 0;
		initEnergy = 2500;
		energy = initEnergy;
	}

	@Override
	public void redraw(MazePanel mazePanel, int state, int px, int py, int view_dx,
			int view_dy, int walk_step, int view_offset, RangeSet rset, int ang) {
		//dbg("redraw") ;
		switch (state) {
		case Constants.STATE_TITLE:
			redrawTitle(mazePanel);
			break;
		case Constants.STATE_GENERATING:
			redrawGenerating(mazePanel);
			break;
		case Constants.STATE_PLAY:
			// skip this one
			break;
		case Constants.STATE_FINISH:
			redrawFinish(mazePanel);
			break;
		case Constants.STATE_DEAD:
			redrawDead(mazePanel);
			break;
		}
	}
	
	private void dbg(String str) {
		System.out.println("MazeView:" + str);
	}
	
	/**
	 * Helper method for redraw to draw the title screen, screen is hardcoded
	 * @param  gc graphics is the off screen image
	 */
	void redrawTitle(MazePanel mazePanel) {
		mazePanel.setColor(Colors.WHITE);
		mazePanel.fillRect(0, 0, Constants.VIEW_WIDTH, Constants.VIEW_HEIGHT);
		mazePanel.setFont(mazePanel.largeBannerFont);
		mazePanel.setColor(Colors.RED);
		centerString(mazePanel, "MAZE", 100);
		mazePanel.setColor(Colors.BLUE);
		mazePanel.setFont(mazePanel.smallBannerFont);
		centerString(mazePanel, "by Paul Falstad", 160);
		centerString(mazePanel, "www.falstad.com", 190);
		mazePanel.setColor(Colors.BLACK);
		centerString(mazePanel, "To start, select a driver type,", 260);
		centerString(mazePanel, "maze generation algorithm, and", 280);
		centerString(mazePanel, "a skill level, then click go!", 300);
		centerString(mazePanel, "v1.2", 350);
	}
	
	/**
	 * Helper method for redraw to draw final screen, screen is hard coded
	 * @param gc graphics is the off screen image
	 */
	void redrawFinish(MazePanel mazePanel) {
		mazePanel.setColor(Colors.BLUE);
		mazePanel.fillRect(0, 0, Constants.VIEW_WIDTH, Constants.VIEW_HEIGHT);
		mazePanel.setFont(mazePanel.largeBannerFont);
		mazePanel.setColor(Colors.YELLOW);
		centerString(mazePanel, "You won!", 100);
		mazePanel.setColor(Colors.ORANGE);
		mazePanel.setFont(mazePanel.smallBannerFont);
		centerString(mazePanel, "Congratulations!", 160);
		mazePanel.setColor(Colors.WHITE);
		centerString(mazePanel, "Hit any key to restart", 250);
		mazePanel.setColor(Colors.ORANGE);
		centerString(mazePanel, "Cell count: " + this.cellCount, 325);
		if (this.driverType == 1) {
			centerString(mazePanel, "Energy consumed: " + (this.energy), 350);
		} else {
		centerString(mazePanel, "Energy consumed: " + (this.initEnergy - this.energy), 350);
		}
	}
	
	/**
	 * Helper method for redraw to draw dead screen, screen is hard coded
	 * @param gc graphics is the off screen image
	 */
	void redrawDead(MazePanel mazePanel) {
		mazePanel.setColor(Colors.BLUE);
		mazePanel.fillRect(0, 0, Constants.VIEW_WIDTH, Constants.VIEW_HEIGHT);
		mazePanel.setFont(mazePanel.largeBannerFont);
		mazePanel.setColor(Colors.YELLOW);
		centerString(mazePanel, "You ran out of", 100);
		centerString(mazePanel, "battery!", 150);
		mazePanel.setColor(Colors.ORANGE);
		mazePanel.setFont(mazePanel.smallBannerFont);
		centerString(mazePanel, ":(", 200);
		mazePanel.setColor(Colors.WHITE);
		centerString(mazePanel, "Hit any key to restart", 250);
		mazePanel.setColor(Colors.ORANGE);
		centerString(mazePanel, "Cell count: " + this.cellCount, 325);
		centerString(mazePanel, "Energy consumed: " + (this.initEnergy - this.energy), 350);
	}

	/**
	 * Helper method for redraw to draw screen during phase of maze generation, screen is hard coded
	 * only attribute percentdone is dynamic
	 * @param gc graphics is the off screen image
	 */
	void redrawGenerating(MazePanel mazePanel) {
		mazePanel.setColor(Colors.YELLOW);
		mazePanel.fillRect(0, 0, Constants.VIEW_WIDTH, Constants.VIEW_HEIGHT);
		mazePanel.setFont(mazePanel.largeBannerFont);
		mazePanel.setColor(Colors.RED);
		centerString(mazePanel, "Building maze", 150);
		mazePanel.setFont(mazePanel.smallBannerFont);
		mazePanel.setColor(Colors.BLACK);
		centerString(mazePanel, maze.getPercentDone()+"% completed", 200);
		centerString(mazePanel, "Hit escape to stop", 300);
	}
	
	private void centerString(MazePanel mazePanel, String str, int ypos) {
		mazePanel.fontMetrics();
		mazePanel.drawString(str, (Constants.VIEW_WIDTH-mazePanel.stringWidth(str))/2, ypos);
	}
	
	/**
	 * Sets the cellCount attribute when the navigation finishes
	 * @param count value to set cellCount to
	 */
	public void setCellCounter(int count) {
		this.cellCount = count;
	}
	
	/**
	 * Sets the energy attribute when the navigation finishes
	 * @param energy value to set energy to
	 */
	public void setEnergy(float energy) {
		this.energy = energy;
	}

	String largeBannerFont = "large";
	String smallBannerFont = "small";
}