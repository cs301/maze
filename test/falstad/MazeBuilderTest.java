package falstad;

import static org.junit.Assert.*;

import org.junit.Test;

public class MazeBuilderTest extends MockMaze {
	
	int width = 8;
	int height = 8;
	MockMaze TestMaze;
	
	/**
	 * Uses deterministic generation (det = true) to create a new MockMaze object with specified skill 
	 * and build it.
	 * @param skill the skill level
	 * @param det true will be deterministic
	 */
	void createMaze(int skill, boolean det) throws InterruptedException{
		this.TestMaze = new MockMaze();
		this.TestMaze.method = 0;
		this.TestMaze.build(skill, det);
		this.TestMaze.mazebuilder.buildThread.join();
	}
	
	/**
	 * Uses random generation to create a new MockMaze object with specified skill and build it.
	 * @param skill the skill level
	 */
	void createMaze(int skill) throws InterruptedException{
		this.TestMaze = new MockMaze();
		this.TestMaze.method = 0;
		this.TestMaze.build(skill);
		this.TestMaze.mazebuilder.buildThread.join();
	}
	
	/**
	 * If a cell is not bounded, the distance between it and the exit will be infinity. 
	 */
	@Test
	public void testIfCellsBounded() throws InterruptedException{
		createMaze(3, true);
		width = this.TestMaze.mazew;
		height = this.TestMaze.mazeh;
		
		for (int i =0; i < width; i++){
			for(int j =0; j < height; j++){
				assertNotSame("Distance is infinity" , (this.TestMaze.dists.getDistance(i, j)), Integer.MAX_VALUE);
				}
		}
	}
	
	/**
	 * Tests to see if the exit position can be reached (not null). 
	 */
	@Test
	public void testExit() throws InterruptedException {
		createMaze(3, true);
		
		int [] exit_pos = null;
		if (this.TestMaze.dists != null) {
			exit_pos = this.TestMaze.dists.getExitPosition();
		}

		assertNotNull(exit_pos);
	}
	
	/**
	 * Tests to see if the cells of two mazes with the same difficulty, when generated with the random
	 * method, are not equal, thus making them unique from each other.
	 */
	@Test
	public void testRandomMode() throws InterruptedException {
		createMaze(3);
		Cells cells1 = this.TestMaze.mazebuilder.getCells();
		
		createMaze(3);
		Cells cells2 = this.TestMaze.mazebuilder.getCells();
		
		assertFalse(cells1.getValueOfCell(5, 5) == cells2.getValueOfCell(5, 5));
	}
	
	/**
	 * Tests to see if the getCells method will return the same value for identical mazes.
	 */
	@Test
	public void testGetCells() throws InterruptedException {
		createMaze(3, true);
		Cells cells1 = this.TestMaze.mazebuilder.getCells();
		
		createMaze(3, true);
		Cells cells2 = this.TestMaze.mazebuilder.getCells();
		
		assertTrue(cells1.getValueOfCell(5, 5) == cells2.getValueOfCell(5, 5));
	}
	
	/**
	 * Tests to see if the getDists method will return the same value for identical mazes.
	 */
	@Test
	public void testGetDists() throws InterruptedException {
		createMaze(3, true);
		Distance dists1 = this.TestMaze.mazebuilder.getDists();
		
		createMaze(3, true);
		Distance dists2 = this.TestMaze.mazebuilder.getDists();
		
		assertTrue(dists1.getDistance(3, 3) == dists2.getDistance(3, 3));
	}
	
	/**
	 * Tests all three cases for return values of the getSign method.
	 */
	@Test
	public void testGetSign() throws InterruptedException {
		createMaze(0, true);
		assertTrue(this.TestMaze.mazebuilder.getSign(3) == 1);
		assertTrue(this.TestMaze.mazebuilder.getSign(-2) == -1);
		assertTrue(this.TestMaze.mazebuilder.getSign(0) == 0);
	}
}