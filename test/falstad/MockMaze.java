package falstad;

public class MockMaze extends Maze {
	BSPNode root;
	Cells cells;
	Distance dists;
	int startx;
	int starty;
	
	@Override
	public void newMaze(BSPNode root, Cells c, Distance dists, int startx, int starty) {
		this.root = root;
		this.cells = c;
		this.dists = dists;
		this.startx = startx;
		this.starty = starty;
	}
	
	public void build(int skill) {
		// select generation method
		switch(method){
		case 2 : mazebuilder = new MazeBuilderAldousBroder(); // generate with AldousBroder's algorithm
		break ;
		case 0: // generate with Falstad's original algorithm (0 and default), note the missing break statement
		default : mazebuilder = new MazeBuilder(); 
		break ;
		}
		
		mazew = Constants.SKILL_X[skill];
		mazeh = Constants.SKILL_Y[skill];
		mazebuilder.build(this, mazew, mazeh, Constants.SKILL_ROOMS[skill], Constants.SKILL_PARTCT[skill]);
		// mazebuilder performs in a separate thread and calls back by calling newMaze() to return newly generated maze
	}

	@Override
	public void init(){	
	}
	
	public void build(int skill, boolean det) {
		// select generation method
		switch(method){
		case 2 : mazebuilder = new MazeBuilderAldousBroder(det); // generate with AldousBroder's algorithm
		break ;
		case 0: // generate with Falstad's original algorithm (0 and default), note the missing break statement
		default : mazebuilder = new MazeBuilder(det); 
		break ;
		}
		
		mazew = Constants.SKILL_X[skill];
		mazeh = Constants.SKILL_Y[skill];
		mazebuilder.build(this, mazew, mazeh, Constants.SKILL_ROOMS[skill], Constants.SKILL_PARTCT[skill]);
		// mazebuilder performs in a separate thread and calls back by calling newMaze() to return newly generated maze
	}
}